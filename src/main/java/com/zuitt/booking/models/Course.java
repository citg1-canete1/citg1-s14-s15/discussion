package com.zuitt.booking.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "courses")
public class Course {

    // Properties
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private double price;
/*
    @ManyToMany(mappedBy = "enrollments")
    @JsonIgnore
    private Set<User> enrollees = new HashSet<>();
*/
    @Column
    private boolean isActive;
    //private Set<User> enrollees;

    // Constructors
    public Course(){}

    public Course(String name, String description, double price) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.isActive = true;
    }

    // Getters and Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
/*
    public Set<User> getEnrollees() {
        return enrollees;
    }

    public void addEnrollee(User user) {
        enrollees.add(user);
        user.getEnrollments().add(this);
    }

    public void removeEnrollee(User user) {
        enrollees.remove(user);
        user.getEnrollments().remove(this);
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public void setEnrollees(Set<User> enrollees) {
    }*/
}
